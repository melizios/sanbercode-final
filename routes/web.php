<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','ArticlesController@index');
Route::group(['middleware' => ['auth']], function (){
    //Route untuk tambah data, edit, delete disini
    Route::get('/articles','ArticlesController@index');
    Route::get('/articles/create','ArticlesController@create');
    Route::post('/articles','ArticlesController@store');
    Route::get('/articles/{articles_id}','ArticlesController@show');
    Route::get('/articles/{articles_id}/edit','ArticlesController@edit');
    Route::put('/articles/{articles_id}','ArticlesController@update');
    Route::delete('/articles/{articles_id}','ArticlesController@destroy');
    Route::get('/profile/{users_id}','ProfileController@show');

    //CRUD TAGS
    Route::get('/tags/create', 'TagsController@create');
    Route::post('/tags', 'TagsController@store');
    Route::get('/tags', 'TagsController@index');
    Route::get('/tags/{tags_id}', 'TagsController@show');
    Route::get('/tags/{tags_id}/edit', 'TagsController@edit');
    Route::put('/tags/{tags_id}', 'TagsController@update');
    Route::delete('/tags/{tags_id}', 'TagsController@destroy');
});
Auth::routes();




