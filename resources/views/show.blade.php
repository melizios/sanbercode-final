@extends('master')
@section('title')
    Detail articles
@endsection
@section('body')
    <h1 style="color: blue;">{{$articles->title}}</h1>
    <p style="text-align: justify;">{!!$articles->content!!}</p><br><br>
    <p>ditulis oleh &emsp;: {{$articles->author}}</p>
    <p>Tags:</p>
    <a href=""></a> 
    @foreach ($tag as $item)
    <span class="badge badge-danger">{{$item->name}}</span>
    @endforeach
    <br>
    <a href="../articles">&#8592;Balikan</a>
@endsection
