@extends('master')
 
@section('title')
    Detail Tags id ke-{{$listTags->id}}
@endsection

@section('body')
<h4>Nama : {{$listTags->name}}</h4>
<h4>Article ID : {{$listTags->articles_id}}</h4>

<a href="/tags">Kembali</a>
@endsection
