@extends('master')
@section('title')
    Edit Tags
@endsection
@section('body')

<div>
    <h2>Edit Tags id ke-{{$tags->id}}</h2>
    <form action="/tags/{{$tags->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$tags->name}}" placeholder="Masukkan Nama Tags">
            @error('name')
                  <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
            @enderror
        </div>

     <div class="form-group">
          <label for="articles_id">Article ID</label>
            <select class="custom-select" name="articles_id" id="articles_id">
                <option value="">..Silahkan Pilih Article ID..</option>
              @foreach ($listID as $item)
                @if ($item->id === $tags->articles_id)
                <option value="{{$item->id}}" selected>{{$item->id}}</option>   
                @else 
                <option value="{{$item->id}}"> {{$item->id}}</option>         
                @endif        
              @endforeach
            </select>
            @error('articles_id')
        <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
           {{ $message }}
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
        </div>
      @enderror
     </div>

        <button type="submit" class="btn btn-primary">Edit</button><br><br>
        <a href="/tags">Batalkan</a>
    </form>
</div>

@endsection