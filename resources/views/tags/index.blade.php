@extends('master')
@section('title')
    List Tags
@endsection
@section('body')

<a href="/tags/create" class="btn btn-primary mb-2">Tambah Tags</a>        {{--diarahkan ke form tambah data --}}
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Article ID</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($listTags as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->name}}</td>
                        <td>{{$value->articles_id}}</td>
                        <td>
                            <form action="/tags/{{$value->id}}" method="POST">
                                <a href="/tags/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/tags/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>           
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        <a href="/articles" class="btn btn-primary mb-2">Kembali</a> 
        @endsection