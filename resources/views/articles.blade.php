@extends('master')
@section('title')
    Daftar articles
@endsection
@section('body')
@auth
<a href="/articles/create" class="btn btn-primary mb-2">Tambah</a>
@endauth
<a href="/tags" class="btn btn-primary mb-2">Lihat Tags</a>


@forelse ($articles as $key=>$value)

<div class="card" style="width: 100%;">
    <div class="card-body text-justify">
        <h5 class="card-title">{{$value->title}}</h5>
        <p class="card-text">{!!Str::limit($value->content,500)!!}</p>
        <p class="card-text">Author: {{$value->author}}</p>
        <a href="/articles/{{$value->id}}" class="btn btn-info">Show</a>
        @auth
            <a href="/articles/{{$value->id}}/edit" class="btn btn-primary mt-1">Edit</a>
            <form action="/articles/{{$value->id}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger my-1" value="Delete">
            </form>
        @endauth
    </div>
</div>
@empty
<div>
    No data
</div>  
@endforelse    


{{-- <table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">title</th>
        <th scope="col">content</th>
        <th scope="col">author</th>
        <th scope="col" >Actions</th>  
      </tr>
    </thead>
    <tbody>
        
        @forelse ($articles as $key=>$value)
            
            <tr>
                <td>{{$value->title}}</td>
                <td>{!!$value->content!!}</td>
                <td>{{$value->author}}</td>
                <td>
                    <a href="/articles/{{$value->id}}" class="btn btn-info">Show</a>
                    @auth
                        <a href="/articles/{{$value->id}}/edit" class="btn btn-primary mt-1">Edit</a>
                        <form action="/articles/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    @endauth
                    
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table> --}}

@endsection
@push('script')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>
@endpush