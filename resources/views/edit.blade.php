@extends('master')
@section('title')
    Edit articles
@endsection
@section('body')
    <div>
        <h2>Edit articles {{$articles->id}}</h2>
        <form action="/articles/{{$articles->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">title</label>
                <input type="text" class="form-control" name="title" value="{{$articles->title}}" id="title" placeholder="Masukkan title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="content">content</label>
                <textarea class="form-control" name="content" id="content" placeholder="Masukkan content" cols="30" rows="10">{{$articles->content}}</textarea>
                @error('content')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        
            <div class="form-group">
                <label for="author">author</label>
                <input type="text" class="form-control" name="author"  value="{{Auth::user()->name}}"  id="author" readonly>
                @error('author')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>

@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/f3tkb7fbup72qwyy0913266pijll4cysfgwon5gbw4o3kbfb/tinymce/5/tinymce.min.js"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });
  </script>
  
@endpush

