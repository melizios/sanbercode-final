<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = "articles";
    protected $fillable = ["title", "content", "author"];
    public $timestamps = false;

    protected function tags(){
        return $this->hasMany('App\Tags', 'articles_id');
    }

}
