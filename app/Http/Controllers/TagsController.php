<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Tags;
use App\Articles;

class TagsController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index']);
    }
    
    // CREATE DATA
    public function create(){
        // $listID = DB::table('articles')->get();
        $listID = Articles::all();
        return view('tags.create', compact('listID'));                    //arahkan ke folder tags file create
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'articles_id' => 'required',
        ]);
        // $query = DB::table('tags')->insert([
        //     "name" => $request["name"],                 
        //     "articles_id" => $request["articles_id"], 
        // ]);

        Tags::create([
    		'name' => $request->name,
            'articles_id' => $request->articles_id
    	]);
        // dd($request->name);
        return redirect('/tags');                      //dikembalikan ke tags
    }


    //TAMPIL DATA
    public function index()
    {
       
        // $listTags = DB::table('tags')->get();          //mengambil data dari database
        // dd($listTags);
        $listTags = Tags::all();
                         
        return view('tags.index', compact('listTags'));           //melempar ke file index
    }


    //TAMPIL DATA BERDASARKAN ID
    public function show($id)
    {
        // $listTags = DB::table('tags')->where('id', $id)->first();
        $listTags = Tags::find($id);
        return view('tags.show', compact('listTags'));
    }


    //UPDATE DATA
    public function edit($id)
    {
        // $tags = DB::table('tags')->where('id', $id)->first();
        $tags = Tags::find($id);
        // $listID = DB::table('articles')->get();
        $listID = Articles::all();
        return view('tags.edit', compact('listID','tags'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'articles_id' => 'required'
        ]);

        // $query = DB::table('tags')
        //     ->where('id', $id)
        //     ->update([
        //         'name' => $request["name"],
        //         'articles_id' => $request["articles_id"],
        //     ]);
        
        $listTags = Tags::find($id);
        $listTags->name = $request->name;
        $listTags->articles_id = $request->articles_id;
    
        $listTags->update();

        return redirect('/tags')->with('succes', 'Berhasil Update');
    }


    //DELETE DATA
    public function destroy($id)
    {
        // $query = DB::table('tags')->where('id', $id)->delete();
        $listTags = Tags::find($id);
        $listTags->delete();
        // dd($listTags);

        return redirect('/tags');
    }
}





