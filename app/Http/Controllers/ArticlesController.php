<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;

use Illuminate\Http\Request;
use DB;
use App\Articles;
use App\Tags;

class ArticlesController extends Controller
{   
    public function __construct(){
        // $this->middleware('auth')->except(['index']);
        $this->middleware('auth')->except(['index','show']);
    }

    public function erdarticles(){
        return view('ERDarticles');
    }

    public function index()
    {
        // $articles = DB::table('articles')->get();
        $articles = Articles::all();
        return view('articles', compact('articles'));
    }
   
    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'title' => 'required|unique:articles',
            'content' => 'required',
            'author' => 'required'
        ]);
        // $query = DB::table('articles')->insert([
        //     "title" => $request["title"],
        //     "content" => $request["content"],
        //     "author" => $request["author"]
        // ]);
        Articles::create([
    		'title' => $request->title,
    		'content' => $request->content,
            'author' => $request->author
    	]);
        
        Alert::success('Berhasil', 'Berhasil menambahkan berita');

        return redirect('/articles');
    }
    
    public function show($id)
    {
        // $articles = DB::table('articles')->where('id', $id)->first();
        $tag = DB::table('tags')->where('articles_id', $id)->get();
        $articles = Articles::find($id);
        return view('show', compact('articles','tag'));
    }
    
    public function edit($id)
    {
        // $articles = DB::table('articles')->where('id', $id)->first();
        $articles = Articles::find($id);
        return view('edit', compact('articles'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'author'  => 'required'
        ]);

        // $query = DB::table('articles')
        //     ->where('id', $id)
        //     ->update([
        //         'title' => $request["title"],
        //         'content' => $request["content"],
        //         'author'  => $request["author"]
        //     ]);
        $articles = Articles::find($id);
        $articles->title = $request->title;
        $articles->content = $request->content;
        $articles->author = $request->author;
        $articles->update();

        Alert::success('Berhasil', 'Berhasil mengupdate berita');


        return redirect('/articles');
    }
    public function destroy($id)
    {
        // $query = DB::table('articles')->where('id', $id)->delete();
        $articles = Articles::find($id);
        // $listTags = Tags::find($id);
        $articles->delete();
        // $listTags->delete();

        return redirect('/articles');
    }
}
