<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ProfileController extends Controller
{
    public function show($id)
    {
        $profile = DB::table('users')->where('id', $id)->first();
        // ->join('users','id','=','profiles.users_id');
        // $profile = DB::table('users')->where('id', $id)->join('profiles','users_id','=','users.id');
        return view('profile', compact('profile'));
    }
}
