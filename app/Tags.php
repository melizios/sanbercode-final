<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = "tags";
    protected $fillable = ["name","articles_id"];
    public $timestamps = false;

    protected function articles(){
        return $this->belongsTo('App\Articles', 'articles_id');
    }
}
